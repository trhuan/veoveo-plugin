<?php
function logo()
{
    if (!$attributes['logo_image']['url']) {
        $logo = get_field('logo', 'option');
        $w = get_field('width_logo', 'option');
        $h = get_field('height_logo', 'option');
    }
?>
    <div class="lth-logo">
        <?php if (is_front_page() || is_home()) { ?>
            <?php if ($logo) { ?>
                <h1>
                    <a href="<?php if (isset($lang)) {
                                    echo get_home_url($lang);
                                } else {
                                    echo get_home_url();
                                } ?>" title="">
                        <img src="<?php echo lth_custom_logo('full', $w, $h); ?>" alt="<?php bloginfo('title'); ?>" width="<?php echo $w; ?>" height="<?php echo $h; ?>">
                    </a>
                    <a href="<?php if (isset($lang)) {
                                    echo get_home_url($lang);
                                } else {
                                    echo get_home_url();
                                } ?>" title="" class="title d-none">
                        <?php bloginfo('title'); ?>
                    </a>
                    <p class="infor d-none"><?php bloginfo('description'); ?></p>
                </h1>
            <?php } else { ?>
                <h1>
                    <a href="<?php if (isset($lang)) {
                                    echo get_home_url($lang);
                                } else {
                                    echo get_home_url();
                                } ?>" title="" class="title">
                        <?php bloginfo('title'); ?>
                    </a>
                    <p class="infor"><?php bloginfo('description'); ?></p>
                </h1>
            <?php }
        } else { ?>
            <?php if ($logo) { ?>
                <a href="<?php if (isset($lang)) {
                                echo get_home_url($lang);
                            } else {
                                echo get_home_url();
                            } ?>" title="">
                    <img src="<?php echo lth_custom_logo('full', $w, $h); ?>" alt="<?php bloginfo('title'); ?>" width="<?php echo $w; ?>" height="<?php echo $h; ?>">
                </a>
                <a href="<?php if (isset($lang)) {
                                echo get_home_url($lang);
                            } else {
                                echo get_home_url();
                            } ?>" title="" class="title d-none">
                    <?php bloginfo('title'); ?>
                </a>
                <p class="infor d-none"><?php bloginfo('description'); ?></p>
            <?php } else { ?>
                <h2>
                    <a href="<?php if (isset($lang)) {
                                    echo get_home_url($lang);
                                } else {
                                    echo get_home_url();
                                } ?>" title="" class="title">
                        <?php bloginfo('title'); ?>
                    </a>
                    <p class="infor"><?php bloginfo('description'); ?></p>
                </h2>
            <?php } ?>
        <?php } ?>
    </div>
<?php }

function megamenu()
{ ?>
    <div class="lth-megamenu megamenu-desktop <?php echo $attributes['class']; ?>  d-none d-lg-block">
        <div class="module_content">
            <div class="menus">
                <?php
                wp_nav_menu(
                    array(
                        'menu'            => 'main_menu',
                        'theme_location'  => 'main_menu',
                        'container'       => '',
                        'container_class' => '',
                        'container_id'    => '',
                        'menu_class'      => 'menu',
                    )
                );
                ?>
            </div>
        </div>
    </div>
<?php }

function megamenu_mobile()
{ ?>
    <div class="lth-megamenu megamenu-mobile <?php echo $attributes['class']; ?>  d-block d-lg-none">
        <div class="open-box">
            <a href="#" title="">
                <span></span>
                <span></span>
                <span></span>
            </a>
        </div>

        <div class="module_content">
            <div class="content-box">
                <div class="close-box">
                    <a href="#" title="" data_toggle="menu-content" class="menu-icon">
                        <i class="fal fa-times"></i>
                    </a>
                </div>

                <div class="menus">
                    <?php
                    wp_nav_menu(
                        array(
                            'menu'            => 'main_menu',
                            'theme_location'  => 'main_menu',
                            'container'       => '',
                            'container_class' => '',
                            'container_id'    => '',
                            'menu_class'      => 'menu',
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php }
