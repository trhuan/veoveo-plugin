<?php
add_action('wp_footer', 'gco_veoveo_footer', 3000);
function gco_veoveo_footer()
{ ?>
    <div class="footer-fixed">
        <?php
        $chats = get_field('chats', 'option');
        $phone = $chats['phone'];
        $zalo = $chats['zalo'];
        $facebook = $chats['facebook'];
        $email = $chats['email'];
        ?>

        <ul>
            <?php if ($facebook) { ?>
                <li class="chat-box chat-facebook-box">
                    <a href="<?php echo $facebook; ?>" target="_blank" title="">
                        <span class="icon-facebook">
                            <i class="fab fa-facebook-square icon"></i>
                        </span>
                    </a>
                </li>
            <?php } ?>

            <?php if ($phone) { ?>
                <li class="chat-box chat-phone-box">
                    <a href="tel:<?php echo $phone; ?>" target="_blank" title="">
                        <span class="icon-phone">
                            <i class="fas fa-phone-alt icon"></i>
                        </span>
                    </a>
                </li>
            <?php } ?>

            <?php if ($zalo) { ?>
                <li class="chat-box chat-zalo-box">
                    <a href="<?php echo $zalo; ?>" target="_blank" title="">
                        <span class="icon-zalo">
                            <span style="font-size: 14px;"><?php echo __('Zalo'); ?></span>
                        </span>
                    </a>
                </li>
            <?php } ?>

            <?php if ($email) { ?>
                <li class="chat-box chat-email-box">
                    <a href="mailto:<?php echo $email; ?>" target="_blank" title="">
                        <span class="icon-email">
                            <i class="fas fa-envelope icon"></i>
                        </span>
                    </a>
                </li>
            <?php } ?>

            <li>
                <div class="back-to-top">
                    <i class="far fa-chevron-up icon" aria-hidden="true"></i>
                </div>
            </li>
        </ul>
    </div>

    <?php $veoveo = get_field('veoveo', 'option');

    $chen_ma_footer = $veoveo['chen_ma_footer'];

    $veoveo_chat_zalo = $veoveo['veoveo_chat_zalo'];
    $veoveo_chat_zalo_url = $veoveo['veoveo_chat_zalo_url'];
    $veoveo_chat_facebook = $veoveo['veoveo_chat_facebook'];
    $veoveo_chat_facebook_url = $veoveo['veoveo_chat_facebook_url'];
    $veoveo_url_theme_single = $veoveo['veoveo_url_theme_single'];
    $veoveo_hottline = $veoveo['veoveo_hottline'];
    $veoveo_fixed_position = $veoveo['veoveo_fixed_position'];

    if ($veoveo_fixed_position != 'none') {
    ?>
        <div class="veoveo-footer-fixed" style="<?php echo $veoveo_fixed_position; ?>: 50px;">
            <?php
            ?>
            <ul>
                <?php if (!empty($veoveo_url_theme_single)) { ?>
                    <li class="chat-box chat-phone-box">
                        <a href="<?php echo $veoveo_url_theme_single; ?>" target="_blank" title="">
                            <span class="icon-phone">
                                <i class="fal fa-link icon"></i>
                            </span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (!empty($veoveo_url_theme_single)) { ?>
                    <li class="chat-box chat-phone-box">
                        <a href="<?php echo $veoveo_url_theme_single; ?>/?buynow" target="_blank" title="">
                            <span class="icon-phone">
                                <i class="fal fa-shopping-cart icon"></i>
                            </span>
                        </a>
                    </li>
                <?php } ?>

                <li class="chat-box chat-pagespeeds">
                    <a target=_blank href="https://pagespeed.web.dev/report?url=<?php echo get_home_url(); ?>%2F&form_factor=desktop">
                        <span class="icon-pagespeed">
                            <i class="fal fa-tachometer-alt-average"></i>
                        </span>
                    </a>
                </li>

                <?php if (empty($veoveo_chat_zalo) && !empty($veoveo_chat_zalo_url)) { ?>
                    <li class="chat-box chat-zalo-box">
                        <a href="<?php echo $veoveo_chat_zalo_url; ?>" target="_blank" title="">
                            <span class="icon-zalo">
                                <!-- <i class="icofont-phone icon"></i> -->
                                <span style="font-family: 'Inter'; font-size: 13px; font-weight: 500;"><?php echo __('Zalo'); ?></span>
                            </span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (empty($veoveo_chat_facebook) && !empty($veoveo_chat_facebook_url)) { ?>
                    <li class="chat-box chat-facebook-box">
                        <a href="<?php echo $veoveo_chat_facebook_url; ?>" target="_blank" title="">
                            <span class="icon-facebook">
                                <i class="fab fa-facebook-messenger icon"></i>
                            </span>
                        </a>
                    </li>
                <?php } ?>

                <?php if (!empty($veoveo_hottline)) { ?>
                    <li class="chat-box chat-phone-box">
                        <a href="tel:<?php echo $veoveo_hottline; ?>" target="_blank" title="">
                            <span class="icon-phone">
                                <i class="fal fa-phone-alt icon"></i>
                            </span>
                        </a>
                    </li>
                <?php } ?>

                <li class="chat-box chat-plus-minus">
                    <a href="#">
                        <span class="icon-plus">
                            <i class="fal fa-arrow-alt-up icon"></i>
                        </span>
                        <span class="icon-minus">
                            <i class="fal fa-minus icon"></i>
                        </span>
                    </a>
                </li>
            </ul>
        </div>

        <?php if (!empty($veoveo_chat_zalo)) {
            echo $veoveo_chat_zalo;
        }

        if (!empty($veoveo_chat_facebook)) {
            echo $veoveo_chat_facebook;
        }

        if (!empty($chen_ma_footer)) {
            echo $chen_ma_footer;
        } ?>
<?php }
    die();
} ?>