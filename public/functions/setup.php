<?php
// Thay doi logo admin wordpress page login
function gco_veoveo_custom_admin_logo()
{
    $veoveo = get_field('veoveo', 'option');
    $veoveo_logo = $veoveo['veoveo_logo'];
    $veoveo_width_logo = $veoveo['veoveo_width_logo'];
    $veoveo_height_logo = $veoveo['veoveo_height_logo'];
    $veoveo_background_image = $veoveo['veoveo_background_image'];
    $developer = $veoveo['developer'];

    if ($veoveo_logo && $developer == 'yes') {
        echo '<style type="text/css">
		body.login {
			background: url(' . $veoveo_background_image . ') no-repeat;
			background-size: cover;
			background-position: center;
		}';
    } elseif (get_field('logo', 'option')) {
        echo '<style type="text/css">
		body.login div#login h1 a {
			background-image: url(' . get_field('logo', 'option') . ') !important;
			background-position: 0 !important;
			background-size: 100% 100%;
			width: ' . get_field('width_logo', 'option') . 'px;
			height: ' . get_field('height_logo', 'option') . 'px;
		}
		</style>';
    }

    if ($veoveo_background_image && $developer == 'yes') {
        echo 'body.login div#login h1 a {
			background-image: url(' . $veoveo_logo . ') !important;
			background-position: 0 !important;
			background-size: 100% 100%;
			width: ' . $veoveo_width_logo . 'px;
			height: ' . $veoveo_height_logo . 'px;
		}
		</style>';
    }
}
add_action('login_enqueue_scripts', 'gco_veoveo_custom_admin_logo');

// Thay logo admin wordpress
function gco_veoveo_remove_logo_and_submenu()
{
    global $wp_admin_bar;
    //the following codes is to remove sub menu
    $wp_admin_bar->remove_menu('about');
    $wp_admin_bar->remove_menu('wporg');
    $wp_admin_bar->remove_menu('documentation');
    $wp_admin_bar->remove_menu('support-forums');
    $wp_admin_bar->remove_menu('feedback');

    $veoveo = get_field('veoveo', 'option');
    $veoveo_logo = $veoveo['veoveo_logo'];
    $developer = $veoveo['developer'];

    if ($veoveo_logo && $developer == 'yes') {
        $wp_admin_bar->add_menu(array(
            'id' => 'wp-logo',
            'title' => '<img src="' . $veoveo_logo . '" style="height: 15px; position: relative; top: 0; background: #fff; padding: 5px;" />',
            'href' => __('#'),
            'meta' => array(
                'title' => __('LTH - Theme by LTH'),
                'tabindex' => 1,
            ),
        ));
    } elseif (get_field('logo', 'option')) {
        //and this is to change wordpress logo
        $wp_admin_bar->add_menu(array(
            'id' => 'wp-logo',
            'title' => '<img src="' . get_field('logo', 'option') . '" alt="Logo"
            width= "' . get_field('width_logo', 'option') . '" height= "' . get_field('height_logo', 'option') . '"
            style="height: 15px; position: relative; top: 0; background: #fff; padding: 5px;" />',
            'href' => __('#'),
            'meta' => array(
                'title' => __('LTH - Theme by LTH'),
                'tabindex' => 1,
            ),
        ));
    }

    //and this is to add new sub menu.
    $wp_admin_bar->add_menu(array(
        'parent' => 'wp-logo',
        'id' => 'sub-menu-id-1',
        'title' => __('About us'),
        'href' => __('#'),
    ));
}
add_action('wp_before_admin_bar_render', 'gco_veoveo_remove_logo_and_submenu');

// Thay favicon admin wordpress
function faviconadmin()
{
    echo '<link rel="Shortcut Icon" type="image/x-icon" href="' . get_field('favicon', 'option') . '" />';
}
add_action('admin_head', 'faviconadmin');

//////////////////////////////

function gco_veoveo_developer_setup()
{
    $veoveo = get_field('veoveo', 'option');
    $developer = $veoveo['developer'];
    if ($developer == 'no') {
        /**
         * Remove Item Menu Admin
         */
        add_action('admin_init', 'gco_veoveo_setting_remove_menu_pages');
        function gco_veoveo_setting_remove_menu_pages()
        {
            remove_menu_page('tools.php');
            remove_menu_page('plugins.php');
            remove_menu_page('edit.php?post_type=acf-field-group');
            remove_submenu_page('options-general.php', 'tinymce-advanced');
            remove_submenu_page('options-general.php', 'ewww-image-optimizer-options');
            remove_submenu_page('options-general.php', 'rlrsssl_really_simple_ssl');
            remove_submenu_page('options-general.php', 'wprocket');
            remove_menu_page('wpcf7');
            remove_menu_page('wpseo_dashboard');
            remove_menu_page('wp-smtp/wp-smtp.php');
            remove_menu_page('premmerce');
        }

        // remove update plugins
        remove_action('load-update-core.php', 'wp_update_plugins');
        add_filter('pre_site_transient_update_plugins', '__return_null');
        define('DISALLOW_FILE_EDIT', true);
        define('DISALLOW_FILE_MODS', true);

        // remove update themes
        remove_action('load-update-core.php', 'wp_update_themes');
        add_filter('pre_site_transient_update_themes', create_function('$a', "return null;"));

        // remove update core wordpress
        add_action('after_setup_theme', 'gco_veoveo_remove_core_updates');
        function gco_veoveo_remove_core_updates()
        {
            if (!current_user_can('update_core')) {
                return;
            }

            //fadd_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );
            add_filter('pre_option_update_core', '__return_null');
            add_filter('pre_site_transient_update_core', '__return_null');
        }

        // xoá chỉnh sửa code theme, plugin trong admin
        define('DISALLOW_FILE_EDIT', true);

        /**
         * Remove Item Admin Bar
         **/
        function remove_wp_logo($wp_admin_bar)
        {
            // $wp_admin_bar->remove_node('comments');
            $wp_admin_bar->remove_node('wp-rocket');
        }
        add_action('admin_bar_menu', 'remove_wp_logo', 999);
    }
}
add_action('after_setup_theme', 'gco_veoveo_developer_setup');
