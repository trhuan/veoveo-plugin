<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://veoveo.com.vn/
 * @since      1.0.1
 *
 * @package    Gco_Veoveo
 * @subpackage Gco_Veoveo/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Gco_Veoveo
 * @subpackage Gco_Veoveo/public
 * @author     GCO <info@veoveo.com.vn>
 */
class Gco_Veoveo_Public
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.1
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.1
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.1
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.1
	 */
	public function enqueue_styles()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gco_Veoveo_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gco_Veoveo_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style('gco-veoveo-Inter', plugin_dir_url(__FILE__) . 'assets/css/Inter.css', array(), 'all');
		wp_enqueue_style('gco-veoveo-bootstrap', plugin_dir_url(__FILE__) . 'assets/css/bootstrap.min.css', array(), 'all');
		wp_enqueue_style('gco-veoveo-fontawesome', plugin_dir_url(__FILE__) . 'assets/css/all.fontawesome.min.css', array(), 'all');

		// 
		wp_enqueue_style('gco-veoveo-main', plugin_dir_url(__FILE__) . 'assets/css/main.css', array(), $this->version, 'all');
		// if (class_exists('WooCommerce')) {
		wp_enqueue_style('gco-veoveo-product', plugin_dir_url(__FILE__) . 'assets/css/product.css', array(), $this->version, 'all');
		// }
		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'assets/css/gco-veoveo-public.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.1
	 */
	public function enqueue_scripts()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gco_Veoveo_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gco_Veoveo_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if (!class_exists('WooCommerce')) {
			// file js (được add vào header - sử dụng khi không dùng woocommerce)	
			wp_enqueue_script('js-gco-veoveo-jquery', plugin_dir_url(__FILE__) . 'assets/js/jquery.min.js', array('jquery'), $this->version, false);
		}

		wp_enqueue_script('js-gco-veoveo-main', plugin_dir_url(__FILE__) . 'assets/js/main.js', array('jquery'), $this->version, false);
		wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'assets/js/gco-veoveo-public.js', array('jquery'), $this->version, false);
	}
}

//////////////////////////////

// add css admin
function addmin_custom_css()
{
	wp_enqueue_style('gco-veoveo-admin', plugin_dir_url(__FILE__) . 'assets/css/admin.css', array(), 'all');
}
add_action('admin_head', 'addmin_custom_css');

require plugin_dir_path(__FILE__) . 'plugins/acf/acf.php';
require plugin_dir_path(__FILE__) . 'plugins/lazy-blocks/lazy-blocks.php';

require plugin_dir_path(__FILE__) . 'functions/theme-options.php';
require plugin_dir_path(__FILE__) . 'functions/theme-options-fields.php';

require plugin_dir_path(__FILE__) . 'functions/setup.php';

require plugin_dir_path(__FILE__) . 'functions/wp-head.php';

require plugin_dir_path(__FILE__) . 'functions/wp-footer.php';

require plugin_dir_path(__FILE__) . 'functions/function.php';

// if (class_exists('WooCommerce')) {
require plugin_dir_path(__FILE__) . 'functions/woocommerce.php';
// }
