<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://veoveo.com.vn/
 * @since      1.0.1
 *
 * @package    Gco_Veoveo
 * @subpackage Gco_Veoveo/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.1
 * @package    Gco_Veoveo
 * @subpackage Gco_Veoveo/includes
 * @author     GCO <info@veoveo.com.vn>
 */
class Gco_Veoveo_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.1
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'gco-veoveo',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
