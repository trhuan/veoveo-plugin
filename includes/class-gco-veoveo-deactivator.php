<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://veoveo.com.vn/
 * @since      1.0.1
 *
 * @package    Gco_Veoveo
 * @subpackage Gco_Veoveo/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.1
 * @package    Gco_Veoveo
 * @subpackage Gco_Veoveo/includes
 * @author     GCO <info@veoveo.com.vn>
 */
class Gco_Veoveo_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.1
	 */
	public static function deactivate() {

	}

}
