<?php

/**
 * Fired during plugin activation
 *
 * @link       https://veoveo.com.vn/
 * @since      1.0.1
 *
 * @package    Gco_Veoveo
 * @subpackage Gco_Veoveo/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.1
 * @package    Gco_Veoveo
 * @subpackage Gco_Veoveo/includes
 * @author     GCO <info@veoveo.com.vn>
 */
class Gco_Veoveo_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.1
	 */
	public static function activate() {

	}

}
